#!/bin/sh

registry=${1:-registry.gitlab.com}
version=${2:-master}

# Clone xtesting repository
git clone https://github.com/onap/integration-xtesting.git
cd integration-xtesting

for docker in healthcheck infra-healthcheck security smoke-usecases-pythonsdk smoke-usecases-robot smoke-usecases-robot-py3
do
  echo " ************************************************"
  echo " ************************************************"
  echo "build the docker $registry/xtesting-$docker:$version"
  echo " ************************************************"
  echo " ************************************************"
  cd $docker
  docker build -t $registry/xtesting-$docker:$version -f ./docker/Dockerfile .
  # push the docker to registry
  echo "Push the docker xtesting-$docker:$version to the gitlab registry"
  docker push $registry/xtesting-$docker:$version
  cd ..
done
